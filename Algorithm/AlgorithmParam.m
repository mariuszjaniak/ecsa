classdef AlgorithmParam < handle
  %Algorithm
  %
  
  properties (GetAccess = public, SetAccess = protected)
    name
  end
  
  methods (Abstract)
    run(obj, task)
  end
  
end

