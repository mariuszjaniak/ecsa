classdef PseudoinverseParam < SingleShootingParam
  %Algorithm
  %
  
  properties (Access = protected)
    sel_
    dim_
    prm_
    sys_
    ctr_
  end
     
  methods
    function obj = PseudoinverseParam(system, control, prm)
      if nargin == 0
        super_args = {};    
      else
        super_args{1} = system;
        super_args{2} = control;
        p = PseudoinverseParam.parseInput(prm);
        super_args{3} = p.Unmatched;
      end
      obj = obj@SingleShootingParam(super_args{:});
      if nargin > 0
        obj.dim_    = system.dim;
        obj.sel_.la = 1 : control.la();
        obj.sel_.p  = control.la() + 1 : control.la() + obj.dim_.p;
        obj.prm_    = p.Results;
        obj.sys_    = system;
        obj.ctr_    = control;
        obj.name    = 'PseudoInverse';
      end
    end
  end
  
  methods (Access = protected)
   
    function init(obj, task)
      obj.state_.t      = [task.Tb task.Tb + task.T];
      obj.state_.tol    = task.tol;
      obj.state_.k      = 0;
      obj.state_.kmax   = task.kmax;
      obj.state_.en     = 0;
      obj.state_.enmin  = task.enmin;
      obj.state_.lambda = task.lambda0;
      obj.state_.p      = task.p0;
      obj.state_.p0     = task.p0;
      obj.state_.q0     = cell(obj.dim_.sys, 1);
      obj.state_.yd     = cell(obj.dim_.sys, 1);
      obj.state_.q0{1}  = task.q0;
      obj.state_.yd{1}  = task.yd;
      obj.state_.tr.e   = cell(obj.dim_.sys, 1);
      obj.state_.tr.en  = cell(obj.dim_.sys, 1);
      for i = 1 : obj.dim_.sys
        if i > 1
          obj.state_.q0{i}  = zeros(obj.dim_.n(i), 1);
          obj.state_.yd{i}  = zeros(obj.dim_.r(i), 1);
        end
        obj.state_.tr.e{i}  = zeros(task.kmax, obj.dim_.r(i));
        obj.state_.tr.en{i} = zeros(task.kmax, 1);
      end

    end
    
    function isEnd = isEnd(obj)
      isEnd = (obj.state_.en < obj.state_.enmin) || ...
              (obj.state_.k  >= obj.state_.kmax);
    end
    
    function eval(obj)
      E = cell(obj.dim_.sys, 1);
      for i = 1 : obj.dim_.sys
        E{i} = obj.state_.sol.y{i} - obj.state_.yd{i};
        obj.state_.tr.e{i}(obj.state_.k + 1, :) = E{i};
        obj.state_.tr.en{i}(obj.state_.k + 1)   = norm(E{i});
      end
      obj.state_.E  = E;
      obj.state_.e  = cell2mat(E);
      obj.state_.en = norm(obj.state_.e);
    end
    
    function iter(obj)
      J                 = cell2mat(obj.state_.sol.J);
      e                 = obj.state_.e;
      X                 = [obj.state_.lambda; obj.state_.p];
      dX                = -pinv(J)*e;
      gamma             = min(1, sqrt(2*obj.state_.tol/norm(dX)));
      X                 = X + gamma*dX;
      obj.state_.J      = J;
      obj.state_.dX     = dX;
      obj.state_.gamma  = gamma;
      obj.state_.lambda = X(obj.sel_.la);
      if obj.dim_.p > 0
        obj.state_.p    = X(obj.sel_.p);
      else
        obj.state_.p    = [];
      end
      obj.state_.k      = obj.state_.k + 1;
    end
        
    function out = out(obj)
      out.dim    = obj.dim_;
      out.lambda = obj.state_.lambda;
      out.p      = obj.state_.p;
      out.yd     = obj.state_.yd;
      out.k      = obj.state_.k;
      out.q0     = obj.state_.q0;
      out.p0     = obj.state_.p0;
      out.t      = obj.state_.sol.t;
      out.q      = obj.state_.sol.q;
      out.e      = obj.state_.tr.e;
      out.en     = obj.state_.tr.en;
      dim.t      = length(out.t);
      out.u      = zeros(dim.t, obj.dim_.m);
      out.y      = cell(obj.dim_.sys, 1);
      for i = 1 : obj.dim_.sys
        out.y{i}  = zeros(dim.t, obj.dim_.r(i));
      end
      dp = (out.p - out.p0)/dim.t;
      for i = 1 : dim.t;
        out.u(i, :) = obj.ctr_.u(out.t(i));
        Q           = cell(obj.dim_.sys, 1);
        for j = 1 : obj.dim_.sys
          Q{j} = out.q{j}(i, :)';
        end
        p = out.p0 + i*dp;
        Y = obj.sys_.k(Q, p);
        for j = 1 : obj.dim_.sys
          out.y{j}(i, :) = Y{j};
        end
      end
    end
    
    function initHook(obj)
      disp('Start')
      if obj.prm_.PlotEn == 1
        obj.state_.fig = figure;
        obj.state_.ax  = axes;
      end
    end
    
    function evalHook(obj)
      msg = [int2str(obj.state_.k), ': '];
      if obj.dim_.sys > 1
        for i = 1 : obj.dim_.sys
          en = norm(obj.state_.E{i});
          msg = [msg, 'en', int2str(i), ' = ', num2str(en), ', ']; %#ok<AGROW>
        end
      end
      msg = [msg, 'en = ', num2str(obj.state_.en)];
      disp(msg);
      if obj.prm_.PlotEn == 1
        plot(obj.state_.ax, obj.state_.q0{1}(obj.prm_.PlotSelQ(1)), ...
          obj.state_.q0{1}(obj.prm_.PlotSelQ(2)), 'k+', 'MarkerSize', 13);
        set(obj.state_.ax,'XLim', obj.prm_.PlotXLim, 'YLim', obj.prm_.PlotYLim);
        hold(obj.state_.ax, 'on');
        plot(obj.state_.ax, ...
          [obj.state_.q0{1}(obj.prm_.PlotSelQ(1)) ...
           obj.state_.q0{1}(obj.prm_.PlotSelQ(1)) + ...
           obj.prm_.PlotVLen*cos(obj.state_.q0{1}(obj.prm_.PlotSelQ(3)))], ...
          [obj.state_.q0{1}(obj.prm_.PlotSelQ(2)) ...
           obj.state_.q0{1}(obj.prm_.PlotSelQ(2)) + ...
           obj.prm_.PlotVLen*sin(obj.state_.q0{1}(obj.prm_.PlotSelQ(3)))], ...
          'k', 'LineWidth', 1);
        plot(obj.state_.ax, ...
          obj.state_.yd{1}(obj.prm_.PlotSelY(1)), ...
          obj.state_.yd{1}(obj.prm_.PlotSelY(2)), ...
          'b--', 'LineWidth', 0.3);
        plot(obj.state_.ax, ...
          obj.state_.sol.q{1}(:, obj.prm_.PlotSelQ(1)), ...
          obj.state_.sol.q{1}(:, obj.prm_.PlotSelQ(2)), ...
          '-k', 'LineWidth', 0.5);
        drawnow;
        hold(obj.state_.ax, 'off');
        if obj.prm_.PlotEps == 1
          xlabel(obj.state_.ax, ['q_', int2str(obj.prm_.PlotSelQ(1))], 'FontSize', 20);
          ylabel(obj.state_.ax, ['q_', int2str(obj.prm_.PlotSelQ(2))], 'FontSize', 20);
          set(obj.state_.ax, 'FontSize', 18);
          print(obj.state_.fig, '-depsc', '-r1200', ...
            [obj.prm_.PlotPathName, '/', obj.prm_.PlotName, '_' ...
            obj.name, '_', obj.sys_.name, '_k', int2str(obj.state_.k), '_path']);
        end
      end
    end
    
    function iterHook(obj) 
      
    end
    
    function outHook(obj)
      disp('End')
    end
  end
  
  methods (Static, Access = protected)
    
    function p = parseInput(prm)
      % Default values 
      def.PlotEn       = 0;
      def.PlotEps      = 0;
      def.PlotVLen     = 0.5;
      def.PlotSelQ     = [1 2 3];
      def.PlotSelY     = [1 2 3];
      def.PlotXLim     = [-10 10];
      def.PlotYLim     = [-10 10];
      def.PlotName     = 'ex';
      def.PlotPathName = '.';
      % Set parser
      p                = inputParser;
      p.CaseSensitive  = true;
      p.StructExpand   = true;
      p.KeepUnmatched  = true;
      
      addParamValue(p, 'PlotEn', def.PlotEn, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotEps', def.PlotEps, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotVLen', def.PlotVLen, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'nonempty'}));
      addParamValue(p, 'PlotSelQ', def.PlotSelQ, ...
        @(x) validateattributes(x, {'double'}, {'numel', 3, 'nonempty', 'integer'}));
      addParamValue(p, 'PlotSelY', def.PlotSelY, ...
        @(x) validateattributes(x, {'double'}, {'numel', 3, 'nonempty', 'integer'}));
      addParamValue(p, 'PlotXLim', def.PlotXLim, ...
        @(x) validateattributes(x, {'double'}, {'numel', 2, 'nonempty'}));
      addParamValue(p, 'PlotYLim', def.PlotYLim, ...
        @(x) validateattributes(x, {'double'}, {'numel', 2, 'nonempty'}));
      addParamValue(p, 'PlotName', def.PlotName, ...
        @(x) validateattributes(x, {'char'}, {'nonempty'}));
      addParamValue(p, 'PlotPathName', def.PlotPathName, ...
        @(x) validateattributes(x, {'char'}, {'nonempty'}));
      parse(p, prm);
    end
  end
  
end
