classdef SingleShootingParam < AlgorithmParam 
  %Algorithm
  %
  
  properties (Access = protected)
    solver_
    state_
  end
  
  methods
    function obj = SingleShootingParam(system, control, prm)
      if nargin > 0
        obj.solver_ = SolverSingleParam(system, control, prm);
      end
    end
    
    function  out = run(obj, task)
      obj.Init(task);
      obj.Eval();
      while ~obj.isEnd()
        obj.Iter();
        obj.Eval();
      end
      out = obj.Out();
    end
    
  end
  
  methods (Access = protected)
    function Init(obj, task)
      obj.init(task);
      obj.initHook();
    end
    
    function Eval(obj)
      obj.state_.sol = obj.solver_.solve(obj.state_.t, obj.state_.q0, ...
        obj.state_.lambda, obj.state_.p);
      obj.eval();
      obj.evalHook()
    end

    function Iter(obj)
      obj.iter();
      obj.iterHook();
    end

    function out = Out(obj)
      out = obj.out();
      obj.outHook();
    end
  end
  
  methods (Abstract, Access = protected)
    isEnd(obj)
    init(obj, task)
    eval(obj)
    iter(obj)
    out(obj)
    initHook(obj)
    evalHook(obj)
    iterHook(obj)
    outHook(obj)
  end

end
