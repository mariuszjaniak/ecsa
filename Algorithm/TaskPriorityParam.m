classdef TaskPriorityParam < SingleShootingParam
  %Algorithm
  %
  
  properties (Access = protected)
    sel_
    dim_
    prm_
    sys_
    ctr_
  end
     
  methods
    function obj = TaskPriorityParam(system, control, prm)
      if nargin == 0
        super_args = {};    
      else
        super_args{1} = system;
        super_args{2} = control;
        p = TaskPriorityParam.parseInput(prm);
        super_args{3} = p.Unmatched;
      end
      obj = obj@SingleShootingParam(super_args{:});
      if nargin > 0
        obj.dim_      = system.dim;
        obj.dim_.la   = control.la();
        obj.sel_.la   = 1 : control.la();
        obj.sel_.p    = control.la() + 1 : control.la() + obj.dim_.p;
        obj.prm_      = p.Results;
        obj.sys_      = system;
        obj.ctr_      = control;
        obj.name      = 'TaskPriority';
        obj.dim_.sch  = length(obj.prm_.Scheme);
        obj.dim_.task = zeros(1, obj.dim_.sch);
        for i = 1 : obj.dim_.sch
          obj.dim_.task(i) = length(obj.prm_.Scheme{i});
        end
      end
    end
  end
  
  methods (Access = protected)
   
    function init(obj, task)
      dim               = obj.dim_;
      obj.state_.t      = [task.Tb task.Tb + task.T];
      obj.state_.tol    = task.tol;
      obj.state_.k      = 0;
      obj.state_.kmax   = task.kmax;
      obj.state_.en     = 0;
      obj.state_.enmin  = task.enmin;
      obj.state_.lambda = task.lambda0;
      obj.state_.p      = task.p0;
      obj.state_.q0     = cell(dim.sys, 1);
      obj.state_.yd     = cell(dim.sys, 1);
      obj.state_.q0{1}  = task.q0;
      obj.state_.yd{1}  = task.yd;
      obj.state_.tr.e   = cell(dim.sys, 1);
      obj.state_.tr.en  = cell(dim.sys, 1);
      for i = 1 : dim.sys
        if i > 1
          obj.state_.q0{i}  = zeros(dim.n(i), 1);
          obj.state_.yd{i}  = zeros(dim.r(i), 1);
        end
        obj.state_.tr.e{i}  = zeros(task.kmax, dim.r(i));
        obj.state_.tr.en{i} = zeros(task.kmax, 1);
      end
      obj.state_.e = cell(dim.sch, 1);
      sch          = obj.prm_.Scheme;
      for i = 1 : dim.sch
        r = 0;
        for j = 1 : dim.task(i)
          r = r + dim.r(sch{i}(j));
        end
        obj.state_.e{i} = zeros(r, 1);
      end
    end
    
    function isEnd = isEnd(obj)
      isEnd = (obj.state_.en < obj.state_.enmin) || ...
              (obj.state_.k  >= obj.state_.kmax);
    end
    
    function eval(obj)
      dim = obj.dim_;
      sch = obj.prm_.Scheme;
      E   = cell(dim.sys, 1);
      k   = obj.state_.k;
      for i = 1 : obj.dim_.sys
        E{i} = obj.state_.sol.y{i} - obj.state_.yd{i};
        obj.state_.tr.e{i}(k + 1, :) = E{i};
        obj.state_.tr.en{i}(k + 1)   = norm(E{i});
      end
      obj.state_.E  = E;
      obj.state_.en = norm(cell2mat(E));
      for i = 1 : dim.sch
        obj.state_.e{i} = cell2mat({E{sch{i}}});
      end
    end
    
    function iter(obj)
      dim          = obj.dim_;
      sel          = obj.sel_;
      sch          = obj.prm_.Scheme;
      e            = obj.state_.e;
      J            = obj.state_.sol.J;
      X            = [obj.state_.lambda; obj.state_.p];
      dX           = 0;
      P            = eye(dim.la + dim.p);
      obj.state_.J = cell(dim.sch, 1);
      for i = 1 : dim.sch
        Ji              = cell2mat({J{sch{i}}});
        ei              = e{i};
        invJi           = pinv(Ji);
        dX              = dX + P*(-invJi*ei);
        Pi              = eye(dim.la + dim.p) - invJi*Ji;
        P               = P*Pi;
        obj.state_.J{i} = Ji;
      end
      gamma             = min(1, sqrt(2*obj.state_.tol/norm(dX)));
      X                 = X + gamma*dX;
      obj.state_.dX     = dX;
      obj.state_.gamma  = gamma;
      obj.state_.lambda = X(sel.la);
      if obj.dim_.p > 0
        obj.state_.p    = X(sel.p);
      else
        obj.state_.p    = [];
      end
      obj.state_.k      = obj.state_.k + 1;
    end
        
    function out = out(obj)
      out.dim    = obj.dim_;
      out.lambda = obj.state_.lambda;
      out.p      = obj.state_.p;
      out.yd     = obj.state_.yd;
      out.k      = obj.state_.k;
      out.q0     = obj.state_.q0;
      out.t      = obj.state_.sol.t;
      out.q      = obj.state_.sol.q;
      out.e      = obj.state_.tr.e;
      out.en     = obj.state_.tr.en;
      dim.t      = length(out.t);
      out.u      = zeros(dim.t, obj.dim_.m);
      out.y      = cell(obj.dim_.sys, 1);
      for i = 1 : obj.dim_.sys
        out.y{i}  = zeros(dim.t, obj.dim_.r(i));
      end
      for i = 1 : dim.t;
        out.u(i, :) = obj.ctr_.u(out.t(i));
        Q           = cell(obj.dim_.sys, 1);
        for j = 1 : obj.dim_.sys
          Q{j} = out.q{j}(i, :)';
        end
        Y = obj.sys_.k(Q, obj.state_.p);
        for j = 1 : obj.dim_.sys
          out.y{j}(i, :) = Y{j};
        end
      end
    end
    
    function initHook(obj)
      disp('Start')
      if obj.prm_.PlotEn == 1
        obj.state_.fig = figure;
        obj.state_.ax  = axes;
      end
    end
    
    function evalHook(obj)
      msg = [int2str(obj.state_.k), ': '];
      if obj.dim_.sys > 1
        for i = 1 : obj.dim_.sys
          en = norm(obj.state_.E{i});
          msg = [msg, 'en', int2str(i), ' = ', num2str(en), ', ']; %#ok<AGROW>
        end
      end
      msg = [msg, 'en = ', num2str(obj.state_.en)];
      disp(msg);
      if obj.prm_.PlotEn == 1
        plot(obj.state_.ax, obj.state_.q0{1}(obj.prm_.PlotSelQ(1)), ...
          obj.state_.q0{1}(obj.prm_.PlotSelQ(2)), 'k+', 'MarkerSize', 13);
        set(obj.state_.ax,'XLim', obj.prm_.PlotXLim, 'YLim', obj.prm_.PlotYLim);
        hold(obj.state_.ax, 'on');
        plot(obj.state_.ax, ...
          [obj.state_.q0{1}(obj.prm_.PlotSelQ(1)) ...
           obj.state_.q0{1}(obj.prm_.PlotSelQ(1)) + ...
           obj.prm_.PlotVLen*cos(obj.state_.q0{1}(obj.prm_.PlotSelQ(3)))], ...
          [obj.state_.q0{1}(obj.prm_.PlotSelQ(2)) ...
           obj.state_.q0{1}(obj.prm_.PlotSelQ(2)) + ...
           obj.prm_.PlotVLen*sin(obj.state_.q0{1}(obj.prm_.PlotSelQ(3)))], ...
          'k', 'LineWidth', 1);
        plot(obj.state_.ax, ...
          obj.state_.yd{1}(obj.prm_.PlotSelY(1)), ...
          obj.state_.yd{1}(obj.prm_.PlotSelY(2)), ...
          'b--', 'LineWidth', 0.3);
        plot(obj.state_.ax, ...
          obj.state_.sol.q{1}(:, obj.prm_.PlotSelQ(1)), ...
          obj.state_.sol.q{1}(:, obj.prm_.PlotSelQ(2)), ...
          '-k', 'LineWidth', 0.5);
        drawnow;
        hold(obj.state_.ax, 'off');
        if obj.prm_.PlotEps == 1
          xlabel(obj.state_.ax, ['q_', int2str(obj.prm_.PlotSelQ(1))], 'FontSize', 20);
          ylabel(obj.state_.ax, ['q_', int2str(obj.prm_.PlotSelQ(2))], 'FontSize', 20);
          set(obj.state_.ax, 'FontSize', 18);
          print(obj.state_.fig, '-depsc', '-r1200', ...
            [obj.prm_.PlotPathName, '/', obj.prm_.PlotName, '_' ...
            obj.name, '_', obj.sys_.name, '_k', int2str(obj.state_.k), '_path']);
        end
      end
    end
    
    function iterHook(~) 
      
    end
    
    function outHook(~)
      disp('End')
    end
  end
  
  methods (Static, Access = protected)
    
    function p = parseInput(prm)
      % Default values 
      def.Scheme       = {1, 2};
      def.PlotEn       = 0;
      def.PlotEps      = 0;
      def.PlotVLen     = 0.5;
      def.PlotSelQ     = [1 2 3];
      def.PlotSelY     = [1 2 3];
      def.PlotXLim     = [-10 10];
      def.PlotYLim     = [-10 10];
      def.PlotName     = 'ex';
      def.PlotPathName = '.';
      % Set parser
      p                = inputParser;
      p.CaseSensitive  = true;
      p.StructExpand   = true;
      p.KeepUnmatched  = true;

      addParamValue(p, 'Scheme', def.Scheme, ...
        @(x) validateattributes(x, {'cell'}, {'nonempty'}));
      addParamValue(p, 'PlotEn', def.PlotEn, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotEps', def.PlotEps, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotVLen', def.PlotVLen, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'nonempty'}));
      addParamValue(p, 'PlotSelQ', def.PlotSelQ, ...
        @(x) validateattributes(x, {'double'}, {'numel', 3, 'nonempty', 'integer'}));
      addParamValue(p, 'PlotSelY', def.PlotSelY, ...
        @(x) validateattributes(x, {'double'}, {'numel', 3, 'nonempty', 'integer'}));
      addParamValue(p, 'PlotXLim', def.PlotXLim, ...
        @(x) validateattributes(x, {'double'}, {'numel', 2, 'nonempty'}));
      addParamValue(p, 'PlotYLim', def.PlotYLim, ...
        @(x) validateattributes(x, {'double'}, {'numel', 2, 'nonempty'}));
      addParamValue(p, 'PlotName', def.PlotName, ...
        @(x) validateattributes(x, {'char'}, {'nonempty'}));
      addParamValue(p, 'PlotPathName', def.PlotPathName, ...
        @(x) validateattributes(x, {'char'}, {'nonempty'}));
      parse(p, prm);
    end
  end
end
