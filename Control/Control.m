classdef Control < handle
  %Control 
  %   
  
  properties (GetAccess = public, SetAccess = protected)
    m
  end
  
  methods
    function obj = Control(m)
      if nargin > 0
        obj.m = m;
      end
    end
   
  end
  
  methods (Abstract)
    u(obj, t)
  end
  
end

