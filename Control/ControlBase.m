classdef ControlBase < handle
  %ControlBase -- control baze P(t)
  
  properties (GetAccess = public, SetAccess = protected)
    s
    m
  end
  
  methods (Abstract)
    P(obj, t)
  end
  
end

