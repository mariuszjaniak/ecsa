classdef ControlBaseFun < ControlBase
  %ControlBaseFun -- control base defined with external function 

  properties
    prm
  end
  
  properties (Access = protected)
    funP_
  end
  
  methods
    function obj = ControlBaseFun(funP, s, prm)
      if nargin > 0
        obj.s     = s;
        obj.m     = length(s);
        obj.funP_ = funP;
        obj.prm   = prm;
      end
    end
    
    function P = P(obj, t)
      P = obj.funP_(t, obj.prm);
    end
  end
end
