classdef ControlParam < Control
  %ControlParam -- Parametrized control 
  %  u = P(t)*lambda

  properties
    lambda
  end
  
  properties (Access = protected)
    base_
    dim_
  end
  
  methods
    function obj = ControlParam(base)
      if nargin == 0
        super_args = {};
      elseif nargin == 1
        super_args{1} = base.m;
      else
        error('Wrong number of input arguments');
      end
      obj = obj@Control(super_args{:});
      if nargin > 0
        obj.base_   = base;
        obj.dim_.la = sum(base.s);
        obj.lambda  = zeros(obj.dim_.la, 1);
      end
    end
    
    function P = P(obj, t)
      P = obj.base_.P(t);
    end
    
    function s = s(obj)
      s = obj.base_.s;
    end

    function la = la(obj)
      la = obj.dim_.la;
    end
    
    function u = u(obj, t)
      u = obj.P(t)*obj.lambda;
    end
    
  end
end

