classdef ControlParamMultiple < ControlParam
  %ControlParam -- Parametrized control for multimple shooting method
  %  u  = P(t)*lambda -- global control
  %  ui = Pi(t)*lambda(i) -- interval control 

  properties (Access = protected)
    basei_
  end
  
  methods
    function obj = ControlParamMultiple(base, basei, N)
      if nargin == 0
        super_args = {};
      elseif nargin == 2
        if base.m == basei.m
          super_args{1} = base;
        else
          error(['Inconsistent control bases dimensions, m = ', ...
            int2str(base.m), ' mi = ', int2str(basei.m)]);
        end
      else
        error('Wrong number of input arguments');
      end
      obj = obj@ControlParam(super_args{:});
      if nargin > 0
        obj.basei_   = basei;
        obj.dim_.lai = sum(basei.s);
      end
    end
    
    function Pi = Pi(obj, t, i)
      %%%%%%%%%%%%%%%%%%%%%%%
      Pi = obj.basei_.P(t);
    end
    
    function si = si(obj)
      si = obj.basei_.s;
    end

    function lai = lai(obj)
      lai = obj.dim_.lai;
    end
    
    function ui = ui(obj, t, i)
      lambdai = 0; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      ui = obj.Pi(t, i)*lambdai;
    end
    
  end
end
