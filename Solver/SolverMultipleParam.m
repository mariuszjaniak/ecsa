classdef SolverMultipleParam
  %SolverMultipleParam -- multiple shooting solver with sensitivity for 
  %                       parametrized controls
  %   
  
  properties (Access = protected)
    sel_
    prm_
  end

  properties (GetAccess = public, SetAccess = protected)
    system
    control
    dim
  end
  
  methods
    function obj = SolverMultipleParam(system, control, prm)
      if nargin > 0
        obj.system  = system;
        obj.control = control;
        sysdim       = obj.system.dim; 
        obj.dim.la   = control.la;
        obj.dim.n    = sum(sysdim.n);
        obj.dim.r    = sum(sysdim.r);
        obj.sel_.q   = 1 : obj.dim.n;
        obj.sel_.J   = obj.dim.n + 1 :  obj.dim.n + obj.dim.n*obj.dim.la;
        idx_q        = 0;
        idx_J        = 0;
        for i = 1 : sysdim.sys
          obj.sel_.sub(i).q = idx_q + 1 : idx_q + sysdim.n(i);
          obj.sel_.sub(i).J = idx_J + 1 : idx_J + sysdim.n(i)*obj.dim.la;
          idx_q = idx_q + sysdim.n(i);
          idx_J = idx_J + sysdim.n(i)*obj.dim.la;
        end
        p = SolverSingleParam.parseInput(prm);
        obj.prm_     = p.Results;
      end
    end
    
    function sol = solve(obj, t, q0, lambda, p)
      obj.control.lambda = lambda;
      sysdim     = obj.system.dim;
      Q0         = cell2mat(q0);
      J0         = zeros(obj.dim.n*obj.dim.la, 1);
      state0     = [Q0; J0];
      fun        = @(t, state)(obj.flow(t, state, p));
      [tt, flow] = ode15s(fun, t, state0, odeset('RelTol', obj.prm_.RelTol));
      Q_tr       = flow(:, obj.sel_.q);
      J          = flow(end, obj.sel_.J);
      Q          = vec2cell(Q_tr(end, :)', {obj.sel_.sub(:).q}, sysdim.sys);
      C          = obj.system.C(Q, p);
      D          = obj.system.D(Q, p);
      Y          = obj.system.k(Q, p);
      sol.t      = tt;
      sol.y      = Y;
      sol.q      = cell(sysdim.sys, 1);
      sol.J      = cell(sysdim.sys, 1);
      for i = 1 : sysdim.sys
        sol.q{i} = Q_tr(:, obj.sel_.sub(i).q);
        Ji       = reshape(J(obj.sel_.sub(i).J), sysdim.n(i), obj.dim.la);
        sol.J{i} = [C{i}*Ji D{i}];
      end
    end
   
  end
  
  methods (Access = private)
    function flow = flow(obj, t, state, p)
      sysdim = obj.system.dim;
      u      = obj.control.u(t);
      Q      = vec2cell(state(obj.sel_.q), {obj.sel_.sub(:).q}, sysdim.sys);
      J      = vec2cell(state(obj.sel_.J), {obj.sel_.sub(:).J}, sysdim.sys);
      DQ     = obj.system.F(t, Q, u, p);
      DJ     = obj.DJ(t, J, Q, u, p);
      flow   = [cell2mat(DQ); cell2mat(DJ)];
    end
    
    function DJ = DJ(obj, t, J, Q, u, p)
      sysdim = obj.system.dim;
      J1     = reshape(J{1}, sysdim.n(1), obj.dim.la);
      A      = obj.system.A(t, Q, u, p);
      B      = obj.system.B(t, Q, u, p);
      P      = obj.control.P(t);
      DJ     = cell(sysdim.sys, 1);
      for i = 1 : sysdim.sys
        DJi   = A{i}*J1 + B{i}*P;
        DJ{i} = reshape(DJi, [], 1);
      end
    end
    
%     function out = vec2cell(obj, in, sel)
%       sysdim = obj.system.dim.sys;
%       out = cell(sysdim, 1);
%       for i = 1 : sysdim
%         out{i} = in(sel{i});
%       end
%     end
  end
  
  methods (Static, Access = protected)
    function p = parseInput(prm)
      % Default values 
      def.RelTol      = 1e-4;
      % Set parser
      p               = inputParser;
      p.CaseSensitive = true;
      p.StructExpand  = true;
      
      addParamValue(p, 'RelTol', def.RelTol, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'nonempty'}));
      if isempty(prm)
        prm = struct;
      end
      parse(p, prm);
    end
  end
end
