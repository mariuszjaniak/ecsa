classdef SummaryBasic < Summary
  %Summary -- experiment summary
  %   
  
  properties (Access = protected)
    prm_
  end
  
  methods
    function obj = SummaryBasic(prm)
      if nargin > 0
        p        = SummaryBasic.parseInput(prm);
        obj.prm_ = p.Results;
      end
    end
    
    function gen(obj, result)
      obj.genPath(result);
      obj.genCtr(result);
      obj.genEn(result);
    end
  end
  
  methods (Access = protected)
    function genPath(obj, result)
      fig = figure;
      ax  = axes;
      set(ax, 'XLim', obj.prm_.PlotXLim, 'YLim', obj.prm_.PlotYLim);
      hold(ax, 'on');
      if obj.prm_.PlotQ
        q = result.q{1};
        plot(ax, q(:, obj.prm_.PlotSelQ(1)), q(:, obj.prm_.PlotSelQ(2)), ...
          '-k', 'LineWidth', 0.7);
      end
      if obj.prm_.PlotY
        y = result.y{1};
        lineType = '-.k';
        if obj.prm_.PlotQ == 0
          lineType = '-k';
        end
        plot(ax, y(:, obj.prm_.PlotSelY(1)), y(:, obj.prm_.PlotSelY(2)), ...
          lineType, 'LineWidth', 0.7);
      end
      if obj.prm_.PlotQ0 == 1
        q0 = result.q0{1};
        plot(ax, q0(obj.prm_.PlotSelQ(1)), q0(obj.prm_.PlotSelQ(2)), ...
          'bo', 'MarkerSize', 13);
        plot(ax, ...
          [q0(obj.prm_.PlotSelQ(1)) q0(obj.prm_.PlotSelQ(1)) + ...
          obj.prm_.PlotVLen*cos(q0(obj.prm_.PlotSelQ(3)))], ...
          [q0(obj.prm_.PlotSelQ(2)) q0(obj.prm_.PlotSelQ(2)) + ...
          obj.prm_.PlotVLen*sin(q0(obj.prm_.PlotSelQ(3)))], ...
          'b', 'LineWidth', 1);
      end      
      if obj.prm_.PlotYd == 1
        yd = result.yd{1};
        plot(ax, yd(obj.prm_.PlotSelY(1)), yd(obj.prm_.PlotSelY(2)), ...
          'go', 'MarkerSize', 13);
        if length(obj.prm_.PlotSelY) == 3
          plot(ax, ...
            [yd(obj.prm_.PlotSelY(1)) yd(obj.prm_.PlotSelY(1)) + ...
            obj.prm_.PlotVLen*cos(yd(obj.prm_.PlotSelY(3)))], ...
            [yd(obj.prm_.PlotSelY(2)) yd(obj.prm_.PlotSelY(2)) + ...
            obj.prm_.PlotVLen*sin(yd(obj.prm_.PlotSelY(3)))], ...
            'g', 'LineWidth', 1);
        end
      end      
      hold(ax, 'off');
      nameq = {['q_', int2str(obj.prm_.PlotSelQ(1))]; 
               ['q_', int2str(obj.prm_.PlotSelQ(2))]};
      namey = {['y_', int2str(obj.prm_.PlotSelY(1))];
               ['y_', int2str(obj.prm_.PlotSelY(2))]};
      if (obj.prm_.PlotQ == 1) && (obj.prm_.PlotY == 0)
        name = nameq;
      elseif (obj.prm_.PlotQ == 0) && (obj.prm_.PlotY == 1)
        name = namey;
      elseif (obj.prm_.PlotQ == 1) && (obj.prm_.PlotY == 1)
        name = cell(2, 1);
        name{1} = [nameq{1}, ', ', namey{1}];
        name{2} = [nameq{2}, ', ', namey{2}];
        legend(ax, 'q', 'y','Location', obj.prm_.PlotLegendQYPos);          
      else
          
      end
      xlabel(ax, name{1}, 'FontSize', 20);
      ylabel(ax, name{2}, 'FontSize', 20);
      if obj.prm_.PlotEps == 1
        set(ax, 'FontSize', 18);
        print(fig, '-depsc', '-r1200', ...
            [obj.prm_.PlotPathName, '/', obj.prm_.PlotName, '_path_final']);
      end
    end
    
    function genCtr(obj, result)
      t   = result.t;
      u   = result.u;
      fig = figure;
      ax  = axes;
      plot(ax, t, u, 'LineWidth', 1);
      box(ax, 'on');
      axis tight;
      xlabel(ax, 't','FontSize',20);
      ylabel(ax, 'u','FontSize',20);
      if obj.prm_.PlotLegendU == 1
        m    = result.dim.m;
        name = cell(m, 1);
        for i = 1 : m
          name{i} = ['u_', int2str(i)];
        end
        legend(ax, name ,'Location', obj.prm_.PlotLegendUPos);
      end
      if obj.prm_.PlotEps == 1
        set(ax,'FontSize',18);
        print(fig, '-depsc','-r1200', ...
          [obj.prm_.PlotPathName, '/', obj.prm_.PlotName, '_u']);
      end      
    end
    
    function genEn(obj, result)
      k   = result.k;
      en  = result.en;
      sys = result.dim.sys;
      fig = figure;
      ax  = axes;
      plot(ax, 0 : k, en{1}(1 : k + 1), 'k-');
      hold(ax, 'on');
      for i = 2 : sys
        plot(ax, 0 : k, en{i}(1 : k + 1), 'b-.');
      end
      hold(ax, 'off');
      box(ax, 'on');
      axis tight;
      xlabel(ax, '\kappa = number of iterations', 'FontSize', 20);
      ylabel(ax, '||e||', 'FontSize', 20);
      if (obj.prm_.PlotLegendEn == 1) && (sys > 1)
        name = cell(sys, 1);
        for i = 1 : sys
          name{i} = ['||e_', int2str(i), '||'];
        end
        legend(ax, name ,'Location', obj.prm_.PlotLegendUPos);
      end
      if obj.prm_.PlotEps == 1
        
        set(ax, 'FontSize', 18);
        print(fig, '-depsc','-r1200', ...
          [obj.prm_.PlotPathName, '/', obj.prm_.PlotName, '_en']);        
      end
    end
    
  end
  
  methods (Static, Access = protected)
    
    function p = parseInput(prm)

      expectedPos = {'North', 'South', 'East', 'West', 'NorthEast', ...
        'NorthWest', 'SouthEast', 'SouthWest', 'NorthOutside', ...
        'SouthOutside', 'EastOutside', 'WestOutside', 'NorthEastOutside', ...
        'NorthWestOutside', 'SouthEastOutside', 'SouthWestOutside', ...
        'Best', 'BestOutside'};
      % Default values 
      def.PlotEps         = 0;
      def.PlotQ0          = 0;
      def.PlotYd          = 0;
      def.PlotQ           = 1;
      def.PlotY           = 1;
      def.PlotLegendU     = 1;
      def.PlotLegendEn    = 1;
      def.PlotLegendUPos  = 'NorthEast';
      def.PlotLegendEnPos = 'NorthEast';
      def.PlotLegendQYPos = 'NorthEast';
      def.PlotVLen        = 0.5;
      def.PlotSelQ        = [1 2 3];
      def.PlotSelY        = [1 2 3];
      def.PlotXLim        = [-10 10]; 
      def.PlotYLim        = [-10 10];
      def.PlotName        = 'ex';
      def.PlotPathName    = '.';
      % Set parser
      p                = inputParser;
      p.CaseSensitive  = true;
      p.StructExpand   = true;
     
      addParamValue(p, 'PlotEps', def.PlotEps, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotQ0', def.PlotQ0, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotYd', def.PlotYd, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotQ', def.PlotQ, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotY', def.PlotY, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotLegendU', def.PlotLegendU, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotLegendEn', def.PlotLegendEn, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'binary', 'nonempty'}));
      addParamValue(p, 'PlotLegendUPos', def.PlotLegendUPos, ...
        @(x) any(validatestring(x, expectedPos)));
      addParamValue(p, 'PlotLegendEnPos', def.PlotLegendEnPos, ...
        @(x) any(validatestring(x, expectedPos)));
      addParamValue(p, 'PlotLegendQYPos', def.PlotLegendQYPos, ...
        @(x) any(validatestring(x, expectedPos)));
      addParamValue(p, 'PlotVLen', def.PlotVLen, ...
        @(x) validateattributes(x, {'double'}, {'scalar', 'nonempty'}));
      addParamValue(p, 'PlotSelQ', def.PlotSelQ, ...
        @(x) validateattributes(x, {'double'}, {'numel', 3, 'nonempty', 'integer'}));
      addParamValue(p, 'PlotSelY', def.PlotSelY, ...
        @(x) validateattributes(x, {'double'}, {'vector', 'nonempty', 'integer'}));
      addParamValue(p, 'PlotXLim', def.PlotXLim, ...
        @(x) validateattributes(x, {'double'}, {'numel', 2, 'nonempty'}));
      addParamValue(p, 'PlotYLim', def.PlotYLim, ...
        @(x) validateattributes(x, {'double'}, {'numel', 2, 'nonempty'}));
      addParamValue(p, 'PlotName', def.PlotName, ...
        @(x) validateattributes(x, {'char'}, {'nonempty'}));
      addParamValue(p, 'PlotPathName', def.PlotPathName, ...
        @(x) validateattributes(x, {'char'}, {'nonempty'}));
      parse(p, prm);
      len = length(p.Results.PlotSelY);
      if (len ~= 2) && (len ~=3)
        error('Parameter PlotSelY must be a row vector with 2 or 3 elements!!!')
      end
    end
  end
end
