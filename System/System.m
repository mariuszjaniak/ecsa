classdef System < handle
  % System -- define system API
  %   
  
  properties (GetAccess = public, SetAccess = protected)
    name
    dim
  end
  
  methods (Abstract)
    F(obj,  t, Q, u, p)
    k(obj, Q, p)
    A(obj, t, Q, u, p)
    B(obj, t, Q, u, p)
    C(obj, Q, p)
    D(obj, Q, p, u)
  end
  
end

