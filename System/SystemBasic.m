classdef SystemBasic < System
  % SystemBasic -- define basic system 
  %   
  
  properties (Access = protected)
    base_
  end
  
  methods
    function obj = SystemBasic(base)
      if nargin > 0
        obj.base_    = base;
        obj.name     = base.name;
        obj.dim.sys =  1;
        dimbase     = obj.base_.dim;
        obj.dim.n   = dimbase.n;
        obj.dim.m   = dimbase.m;
        obj.dim.p   = dimbase.p;
        obj.dim.r   = dimbase.r;
      end
    end
  end
  
  methods
    function DQ = F(obj, t, Q, u, p)
      q     = Q{1};
      DQ    = cell(1);
      DQ{1} = obj.base_.f(t, q, u);
    end
    
    function Y = k(obj, Q, p)
      q    = Q{1};
      Y    = cell(1);
      Y{1} = obj.base_.k(q, p);
    end
    
    function A = A(obj, t, Q, u, p)
      q    = Q{1};
      A    = cell(1);
      A{1} = obj.base_.A(t, q, u);
    end
    
    function B = B(obj, t, Q, u, p)
      q    = Q{1};
      B    = cell(1);
      B{1} = obj.base_.B(t, q, u);
    end
        
    function C = C(obj, Q, p)
      q    = Q{1};
      C    = cell(1);
      C{1} = obj.base_.C(q, p);
    end
    
    function D = D(obj, Q, p)
      q    = Q{1};
      D    = cell(1);
      D{1} = obj.base_.D(q, p);
    end
    
  end
end

