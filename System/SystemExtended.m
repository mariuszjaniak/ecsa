classdef SystemExtended < System
  % System -- define system 
  %   
  
  properties (Access = protected)
    base_
    associated_
  end
  
  methods
    function obj = SystemExtended(base, varargin)
      if nargin > 0
        obj.base_    = base;
        obj.dim.sys = 1;
        for i = 2 : nargin
          obj.associated_{i - 1} = varargin{i - 1};
          obj.dim.sys = obj.dim.sys + 1;
        end
        dimbase         = obj.base_.dim;
        obj.dim.n       = zeros(obj.dim.sys, 1);
        obj.dim.n(1, 1) = dimbase.n;
        obj.dim.m       = dimbase.m;
        obj.dim.p       = dimbase.p;
        obj.dim.r       = zeros(obj.dim.sys, 1);
        obj.dim.r(1, 1) = dimbase.r;
        for i = 2 : obj.dim.sys
          associated  = obj.associated_{i - 1}.dim;
          obj.dim.n(i, 1) = associated.n;
          obj.dim.r(i, 1) = associated.r;
        end
        obj.name = ['Extended', base.name];
      end
    end
  end
  
  methods
    function DQ = F(obj, t, Q, u, p)
      q     = Q{1};
      DQ    = cell(obj.dim.sys, 1);
      DQ{1} = obj.base_.f(t, q, u);
      for i = 2 : obj.dim.sys
        DQ{i} = obj.associated_{i - 1}.g(t, q, u, p);
      end
    end
    
    function Y = k(obj, Q, p)
      q    = Q{1};
      Y    = cell(obj.dim.sys, 1);
      Y{1} = obj.base_.k(q, p);
      for i = 2 : obj.dim.sys
        x    = Q{i};
        Y{i} = obj.associated_{i - 1}.k(x, p);
      end
    end
    
    function A = A(obj, t, Q, u, p)
      q    = Q{1};
      A    = cell(obj.dim.sys, 1);
      A{1} = obj.base_.A(t, q, u);
      for i = 2 : obj.dim.sys
        A{i} = obj.associated_{i - 1}.A(t, q, u, p);
      end
      
    end
    
    function B = B(obj, t, Q, u, p)
      q    = Q{1};
      B    = cell(obj.dim.sys, 1);
      B{1} = obj.base_.B(t, q, u);
      for i = 2 : obj.dim.sys
        B{i} = obj.associated_{i - 1}.B(t, q, u, p);
      end
    end
        
    function C = C(obj, Q, p)
      q    = Q{1};
      C    = cell(obj.dim.sys, 1);
      C{1} = obj.base_.C(q, p);
      for i = 2 : obj.dim.sys
        x    = Q{i};
        C{i} = obj.associated_{i - 1}.C(x, p);
      end
    end
    
    function D = D(obj, Q, p)
      q    = Q{1};
      D    = cell(obj.dim.sys, 1);
      D{1} = obj.base_.D(q, p);
      for i = 2 : obj.dim.sys
        x    = Q{i};
        D{i} = obj.associated_{i - 1}.D(x, p);
      end
    end
  end
  
end

