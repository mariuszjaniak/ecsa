classdef ControlLim < SystemAssociate
  % ControlLim -- Control limits 
  %   
  
  properties (Access = protected)
    sel_
    lb_
    ub_
    prm_
  end
  
  methods
    function obj = ControlLim(base, sel, lb, ub, prm)
      if nargin > 0
        obj.sel_       = sel;
        obj.lb_        = lb;
        obj.ub_        = ub;
        obj.prm_       = prm;
        obj.dim.n      = 1;
        obj.dim.r      = 1;
        dim            = base.dim();
        obj.dim.base.n = dim.n;
        obj.dim.base.m = dim.m;
        obj.dim.base.p = dim.p;
      end
    end
    
    function Dx = g(obj, ~, ~, u, ~)
      us = u(obj.sel_);
      K  = obj.prm_.K;
      Dx = Mangasarian(obj.lb_ - us, K) + Mangasarian(us - obj.ub_, K);
    end
    
    function y = k(~, x, ~)
      y = x;
    end
    
    function A = A(obj, ~, ~, ~, ~)
      A = zeros(obj.dim.n, obj.dim.base.n);
    end
    
    function B = B(obj, ~, ~, u, ~)
      us  = u(obj.sel_);
      K   = obj.prm_.K;
      reg = obj.prm_.reg;
      B   = zeros(obj.dim.n, obj.dim.base.m);
      B(1, obj.sel_) = 2*(atan(us).*(1./(1 + us.*us)))*reg*piecewise(us, obj.lb_, obj.ub_) + ...
        (1./(1 + exp(-K*(us - obj.ub_)))) - ...
        (1./(1 + exp(-K*(-us + obj.lb_))));
    end
    
    function C = C(obj, ~, ~)
      C = eye(obj.dim.n);
    end
    
    function D = D(obj, ~, ~)
      dim = obj.dim;
      if dim.base.p == 0
        D = [];
      else
        D = zeros(dim.r, dim.base.p);
      end
    end
  end
end
