classdef StateLim < SystemAssociate
  % StateLim -- State limits 
  %   
  
  properties (Access = protected)
    sel_
    lb_
    ub_
    prm_
  end
  
  methods
    function obj = StateLim(base, sel, lb, ub, prm)
      if nargin > 0
        obj.sel_       = sel;
        obj.lb_        = lb;
        obj.ub_        = ub;
        obj.prm_       = prm;
        obj.dim.n      = 1;
        obj.dim.r      = 1;
        dim            = base.dim();
        obj.dim.base.n = dim.n;
        obj.dim.base.m = dim.m;
        obj.dim.base.p = dim.p;
      end
    end
    
    function Dx = g(obj, ~, q, ~, ~)
      qs = q(obj.sel_);
      K  = obj.prm_.K;
      Dx = Mangasarian(obj.lb_ - qs, K) + Mangasarian(qs - obj.ub_, K);
    end
    
    function y = k(~, x, ~)
      y = x;
    end
    
    function A = A(obj, ~, q, ~, ~)
      qs  = q(obj.sel_);
      K   = obj.prm_.K;
      reg = obj.prm_.reg;
      A   = zeros(obj.dim.n, obj.dim.base.n);
      A(1, obj.sel_) = 2*qs*reg*piecewise(qs, obj.lb_, obj.ub_) + ...
        (1./(1 + exp(-K*(qs - obj.ub_)))) - ...
        (1./(1 + exp(-K*(-qs + obj.lb_))));
    end
    
    function B = B(obj, ~, ~, ~, ~)
      B = zeros(obj.dim.n, obj.dim.base.m);
    end
    
    function C = C(obj, ~, ~)
      C = eye(obj.dim.n);
    end
    
    function D = D(obj, ~, ~)
      dim = obj.dim;
      if dim.base.p == 0
        D = [];
      else
        D = zeros(dim.r, dim.base.p);
      end
    end
  end
end
