classdef SystemAssociate < handle
  % SystemAssociate -- define associated system API
  %   
  % dot(q) = f(q, u) -- system base
  % dot(x) = g(q, u)
  % y      = k(x, p)
  
  properties (GetAccess = public, SetAccess = protected)
    dim
  end
  
  methods (Abstract)
    g(obj, t, q, u, p)
    k(obj, x, p)
    A(obj, t, q, u, p)
    B(obj, t, q, u, p)
    C(obj, x, p)
    D(obj, x, p)
  end
  
end

