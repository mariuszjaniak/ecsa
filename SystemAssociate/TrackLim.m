classdef TrackLim < SystemAssociate
  % TrackLim -- Tracking limits 
  %   
  
  properties (Access = protected)
    sel_
    prm_
    trajgen_
  end
  
  methods
    function obj = TrackLim(base, sel, trajgen, prm)
      if nargin > 0
        obj.sel_       = sel;
        obj.trajgen_   = trajgen;
        obj.prm_       = prm;
        obj.dim.n      = length(sel);
        obj.dim.r      = length(sel);
        dim            = base.dim();
        obj.dim.base.n = dim.n;
        obj.dim.base.m = dim.m;
        obj.dim.base.p = dim.p;
      end
    end
    
    function Dx = g(obj, t, q, ~, p)
      y  = obj.kt(q, p);
      yd = obj.trajgen_(t, q, obj.prm_);
      Dx = (y - yd).*(y - yd);
    end
    
    function y = k(~, x, ~)
      y = x;
    end
    
    function A = A(obj, ~, q, ~, p)
      y    = obj.kt(q, p);
      yd   = obj.trajgen_(t, q, obj.prm_);
      dydq = obj.dkfdq(q, p);
      e    = 2*(y - yd);
      r    = obj.dim.r;
      n    = obj.dim.base.n;
      A    = zeros(r, n);
      for i = 1 : r
        A(i, :) = e(i)*dydq(i, :);
      end
    end
    
    function B = B(obj, ~, ~, ~, ~)
      B = zeros(obj.dim.n, obj.dim.base.m);
    end
    
    function C = C(obj, ~, ~)
      C = eye(obj.dim.n);
    end
    
    function D = D(obj, ~, ~)
      dim = obj.dim;
      if dim.base.p == 0
        D = [];
      else
        D = zeros(dim.r, dim.base.p);
      end
    end
  end
  
  methods (Access = private)
    function y = kt(obj, q, ~)
      y = q(obj.sel_);
    end
    
    function dkdq = dktdq(obj, ~, ~)
      r     = obj.dim.r;
      n     = obj.dim.base.n;
      dkdq  = zeros(r, n);
      dkdq(:, obj.sel_) = eye(r);
    end
  end
end
