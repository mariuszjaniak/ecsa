classdef Ball < SystemBase
  % Ball -- rolling ball
  %   Detailed explanation goes here

  properties (Access = protected)
    prm_
  end
  
  methods
    function obj = Ball(prm)
      if nargin > 0
        obj.dim.n = 5;
        obj.dim.m = 2;
        obj.dim.p = 0;
        obj.dim.r = length(prm.track);
        obj.prm_  = prm;
        obj.name  = 'Ball';
      end
    end
    function Dq = f(obj, ~, q, u)
      Dq = obj.G(q)*u;
    end
    
    function y = k(obj, q, ~)
      y = q(obj.prm_.track);
    end
    
    function A = A(~, ~, q, u)
      A      = zeros(5);
      A(1,4) =  u(1)*cos(q(4))*sin(q(5));
      A(1,5) =  u(1)*sin(q(4))*cos(q(5)) - u(2)*sin(q(5));
      A(2,4) = -u(1)*cos(q(4))*cos(q(5));
      A(2,5) =  u(1)*sin(q(4))*sin(q(5)) + u(2)*cos(q(5));
      A(5,4) =  u(1)*sin(q(4));
    end
    
    function B = B(obj, ~, q, ~)
       B = obj.G(q);
    end
    
    function C = C(obj, ~, ~)
      r  = obj.dim.r;
      C  = zeros(r, 5);
      C(1 : r, obj.prm_.track) = eye(r);
    end
    
    function D = D(~, ~, ~)
      D = [];
    end
  end
  
  methods (Access = private)
    function G = G(~, q)
      G      = zeros(5, 2);
      G(1,1) =  sin(q(4))*sin(q(5));
      G(1,2) =  cos(q(5));
      G(2,1) = -sin(q(4))*cos(q(5));
      G(2,2) =  sin(q(5));
      G(3,1) =  1;
      G(4,2) =  1;
      G(5,1) = -cos(q(4));
    end
    
  end
  
end

