classdef Car < SystemBase
  % Car -- kinematic car mobile platform
  %   Detailed explanation goes here

  properties (Access = protected)
    prm_
  end
  
  methods
    function obj = Car(prm)
      if nargin > 0
        obj.dim.n = 4;
        obj.dim.m = 2;
        obj.dim.p = 0;
        obj.dim.r = length(prm.track);
        obj.prm_   = prm;
        obj.name   = 'Car';
      end
    end
    function Dq = f(obj, ~, q, u)
      Dq = obj.G(q)*u;
    end
    
    function y = k(obj, q, ~)
      y = q(obj.prm_.track);
    end
    
    function A = A(~, ~, q, u)
      A      = zeros(4);
      A(1,3) = -u(1)*sin(q(3))*cos(q(4));
      A(1,4) = -u(1)*cos(q(3))*sin(q(4));
      A(2,3) =  u(1)*cos(q(3))*cos(q(4));
      A(2,4) = -u(1)*sin(q(3))*sin(q(4));
      A(3,4) =  u(1)*cos(q(4));
    end
    
    function B = B(obj, ~, q, ~)
       B = obj.G(q);
    end
    
    function C = C(obj, ~, ~)
      r  = obj.dim.r;
      C  = zeros(r, 4);
      C(1 : r, obj.prm_.track) = eye(r);
    end
    
    function D = D(~, ~, ~)
      D = [];
    end
  end
  
  methods (Access = private)
    function G = G(~, q)
      G      = zeros(4, 2);
      G(1,1) = cos(q(3))*cos(q(4));
      G(2,1) = sin(q(3))*cos(q(4));
      G(3,1) = sin(q(4));
      G(4,2) = 1;
    end
    
  end
  
end

