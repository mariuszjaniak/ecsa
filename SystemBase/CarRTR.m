classdef CarRTR < SystemBase
  % CarRTR -- kinematic car mobile platform with RTR onbaoard manipulator 
  %   Detailed explanation goes here

  properties (Access = protected)
    prm_
  end
  
  methods
    function obj = CarRTR(prm)
      if nargin > 0
        obj.dim.n = 4;
        obj.dim.m = 2;
        obj.dim.p = 3;
        obj.dim.r = 3;
        obj.prm_   = prm;
        obj.name   = 'CarRTR';
      end
    end
    function Dq = f(obj, ~, q, u)
      Dq = obj.G(q)*u;
    end
    
    function y = k(obj, q, p)
      l = obj.prm_.l;
      y = [ q(1) + (l(2) + l(3)*cos(p(3)))*cos(q(3) + p(1));
            q(2) + (l(2) + l(3)*cos(p(3)))*sin(q(3) + p(1));
            p(2) + l(3)*sin(p(3)) ];
    end
    
    function A = A(~, ~, q, u)
      A      = zeros(4);
      A(1,3) = -u(1)*sin(q(3))*cos(q(4));
      A(1,4) = -u(1)*cos(q(3))*sin(q(4));
      A(2,3) =  u(1)*cos(q(3))*cos(q(4));
      A(2,4) = -u(1)*sin(q(3))*sin(q(4));
      A(3,4) =  u(1)*cos(q(4));
    end
    
    function B = B(obj, ~, q, ~)
       B = obj.G(q);
    end
    
    function C = C(obj, q, p)
      r      = obj.dim.r;
      l      = obj.prm_.l;
      C      = zeros(r, 4);
      C(1,1) =  1;
      C(1,3) = -(l(2)+l(3)*cos(p(3)))*sin(q(3) + p(1));
      C(2,2) =  1;
      C(2,3) =  (l(2)+l(3)*cos(p(3)))*cos(q(3) + p(1));
    end
    
    function D = D(obj, q, p)
      dim    = obj.dim;
      l      = obj.prm_.l;
      D      = zeros(dim.r, dim.p);
      D(1,1) = -(l(2) + l(3)*cos(p(3)))*sin(q(3) + p(1));
      D(1,3) = -l(3)*sin(p(3))*cos(q(3) + p(1));
      D(2,1) =  (l(2) + l(3)*cos(p(3)))*cos(q(3) + p(1));
      D(2,3) = -l(3)*sin(p(3))*sin(q(3) + p(1));
      D(3,2) =  1;
      D(3,3) =  l(3)*cos(p(3));
    end
  end
  
  methods (Access = private)
    function G = G(~, q)
      G      = zeros(4, 2);
      G(1,1) = cos(q(3))*cos(q(4));
      G(2,1) = sin(q(3))*cos(q(4));
      G(3,1) = sin(q(4));
      G(4,2) = 1;
    end
    
  end
  
end

