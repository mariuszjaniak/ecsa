classdef SystemBase < handle
  % System_base -- define base system 
  %   
  % dot(q) = f(q, u) 
  % y      = k(x, p)

  properties (GetAccess = public, SetAccess = protected)
    name
    dim
  end
  
  methods (Abstract)
    f(obj, t, q, u)
    k(obj, q, p)
    A(obj, t, q, u)
    B(obj, t, q, u)
    C(obj, q, p)
    D(obj, q, p)
  end
  
end

