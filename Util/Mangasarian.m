function M = Mangasarian(x, k)
% Mangasarian function , where a larger k
% corresponds to a sharper transition at x = 0
kx   = k*x;
% Treshold for Inf removal
tres = 1e+02;
% Remove Inf
Infrem = -kx < 1e+02;
kx = kx.*Infrem + (1 - Infrem)*-tres;
x  = x.*Infrem + (1 - Infrem).*(-tres/k);
% Mangasarian
M = x + 1/k*log(1 + exp(-kx));

