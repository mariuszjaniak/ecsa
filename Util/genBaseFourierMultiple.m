function base = genBaseFourierMultiple(s, N)

% Create mat_P.m and mat_Pi.m files defining matrix P with the Fourier base for
% a multiple shooting method.

clear mat_P;       %usun z pamieci funkcje mat_P
clear mat_Pi;      %usun z pamieci funkcje mat_Pi

dim.m = length(s);

% Creat mat_P
fid=fopen('mat_P.m','w');

fprintf(fid,'function P = mat_P(t, prm)\n\n');

fprintf(fid,'%% Fourier base\n');
fprintf(fid,'%%  t   - time\n');
fprintf(fid,'%%  prm - parameters\n\n');

fprintf(fid,'%% Start time\n');
fprintf(fid,'Tb = prm.Tb;\n');
fprintf(fid,'%% Time horizon\n');
fprintf(fid,'T  = prm.T;\n');
fprintf(fid,'%% Time slot\n');
fprintf(fid,'Ti = T/%d;\n', N);
fprintf(fid,'%% Time offset\n');
fprintf(fid,'ti = Tb : Ti : T + Tb; \n');
if sum(s) ~= 0
  fprintf(fid,'%% Pulsation\n');
  fprintf(fid,'omega = 2*pi/Ti;\n\n');
end
fprintf(fid,'%% Activation function\n');
for j = 1 : N
  fprintf(fid,'En%d = piecewise(t, ti(%d), ti(%d));\n', j, j, j+1);
end
fprintf(fid,'%% Base matrix\n');
for j = 1 : dim.m
   for k = 1 : N
     fprintf(fid,'P%d_%d = En%d*[', j, k, k);
     sincos = 0;
     harm   = 1;
     for l = 1 : s(j)
       if l == 1
         fprintf(fid, ' 1');
       else
         if sincos == 0
           fprintf(fid,' sin(%d*omega*(t - ti(%d)))', harm, k);
           sincos = 1;
         else
           fprintf(fid,' cos(%d*omega*(t - ti(%d)))', harm, k);
           sincos = 0;
           harm = harm + 1;
         end
       end
     end
     fprintf(fid,' ];\n');
   end
end
for j = 1 : dim.m
  fprintf(fid,'P%d = [', j);
  for k = 1 : N
    fprintf(fid,' P%d_%d', j, k);
  end
  fprintf(fid,' ];\n');
end
fprintf(fid,'%% Block diagonal matrix\n');
for j = 1 : dim.m
  if j == 1
    fprintf(fid,'P = [');
  else
    fprintf(fid,'     ');
  end
  for k = 1 : dim.m
    if j==k
      fprintf(fid,'P%d          ',j);
    else
      fprintf(fid,'zeros(1,%d) ', s(k)*N);
    end
  end
  if j ~= dim.m
    fprintf(fid,';\n');
  end
end
fprintf(fid,'];\n');

fclose(fid);

% Creat mat_Pi
fid=fopen('mat_Pi.m','w');

fprintf(fid,'function Pi = mat_Pi(t, prm)\n\n');

fprintf(fid,'%% Fourier base\n');
fprintf(fid,'%%  t   - time\n');
fprintf(fid,'%%  prm - parameters\n\n');

fprintf(fid,'%% Start time\n');
fprintf(fid,'Tb = prm.Tb;\n');
fprintf(fid,'%% Time horizon\n');
fprintf(fid,'T  = prm.T;\n');
if sum(s) ~= 0
  fprintf(fid,'%% Pulsation\n');
  fprintf(fid,'omega = 2*pi/T;\n');
end
fprintf(fid,'%% Activation function\n');
fprintf(fid,'En = piecewise(t, Tb, Tb + T);\n');
fprintf(fid,'%% Base matrix\n');
for j = 1 : dim.m
  fprintf(fid,'P%d = En*[', j);
  sincos = 0;
  harm   = 1;
  for k = 1 : s(j)
    if k == 1
      fprintf(fid, ' 1');
    else
      if sincos == 0
        fprintf(fid,' sin(%d*omega*(t - Tb))', harm);
        sincos = 1;
      else
        fprintf(fid,' cos(%d*omega*(t - Tb))', harm);
        sincos = 0;
        harm = harm + 1;
      end
    end
  end
  fprintf(fid,' ];\n');
end
fprintf(fid,'%% Block diagonal matrix\n');
for j = 1 : dim.m
  if j == 1
    fprintf(fid,'Pi = [');
  else
    fprintf(fid,'      ');
  end
  for k = 1 : dim.m
    if j==k
      fprintf(fid,'P%d         ',j);
    else
      fprintf(fid,'zeros(1,%d) ', s(k));
    end
  end
  if j ~= dim.m
    fprintf(fid,';\n');
  end
end
fprintf(fid,'];\n');

fclose(fid);

rehash path
base = 'fourier';
