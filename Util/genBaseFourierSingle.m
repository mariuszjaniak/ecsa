function base = genBaseFourierSingle(s)

% Create mat_P.m file defining matrix P with the Fourier base for a single 
% shooting method.

clear mat_P;

dim.m = length(s);

fid = fopen('mat_P.m','w');

fprintf(fid,'function P = mat_P(t, prm)\n\n');

fprintf(fid,'%% Fourier base\n');
fprintf(fid,'%%  t   - time\n');
fprintf(fid,'%%  prm - parameters\n\n');

fprintf(fid,'%% Start time\n');
fprintf(fid,'Tb = prm.Tb;\n');

if sum(s) ~= 0
  fprintf(fid,'%% Pulsation\n');
  fprintf(fid,'omega = 2*pi/prm.T;\n\n');
end
fprintf(fid,'%% Base matrix\n');
for j = 1 : dim.m
  fprintf(fid,'P%d = [', j);
  sincos = 0;
  harm   = 1;
  for k = 1 : s(j)
    if k == 1
      fprintf(fid, ' 1');
    else
      if sincos == 0
        fprintf(fid,' sin(%d*omega*(t - Tb))', harm);
        sincos = 1;
      else
        fprintf(fid,' cos(%d*omega*(t - Tb))', harm);
        sincos = 0;
        harm = harm + 1;
      end
    end
  end
  fprintf(fid,' ];\n');
end
fprintf(fid,'%% Block diagonal matrix\n');
for j = 1 : dim.m
  if j == 1
    fprintf(fid,'P = [');
  else
    fprintf(fid,'     ');
  end
  for k = 1 : dim.m
    if j==k
      fprintf(fid,'P%d         ',j);
    else
      fprintf(fid,'zeros(1,%d) ', s(k));
    end
  end
  if j ~= dim.m
    fprintf(fid,';\n');
  end
end
fprintf(fid,'];\n');

fclose(fid);
rehash path
base = 'fourier';
