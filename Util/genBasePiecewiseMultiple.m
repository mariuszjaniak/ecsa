function base = genBasePiecewiseMultiple(s, N)

% Create mat_P.m and mat_Pi.m files defining matrix P with the piecewise 
% constant base for multiple shooting method.

clear mat_P;
clear mat_Pi;

dim.m = length(s);

% Creat mat_P
fid = fopen('mat_P.m', 'w');

fprintf(fid, 'function P = mat_P(t, prm)\n\n');

fprintf(fid, '%% Piecewise constant base\n');
fprintf(fid, '%%  t   - time\n');
fprintf(fid, '%%  prm - parameters\n\n');

fprintf(fid, '%% Start time\n');
fprintf(fid, 'Tb = prm.Tb;\n');
fprintf(fid, '%% Time horizon\n');
fprintf(fid, 'T  = prm.T;\n');
fprintf(fid, '%% Number of intervals\n');
for j = 1 : dim.m
  fprintf(fid, 'h%d = T/%d; \n', j, s(j)*N);
end
fprintf(fid, '\n');

fprintf(fid, '%% Base function odffset\n');
for j = 1 : dim.m
  fprintf(fid, 't%d = Tb : h%d : T + Tb; \n', j, j);
end
fprintf(fid,'\n');

fprintf(fid, '%% Base matrix\n');
for j = 1 : dim.m
  fprintf(fid, 'P%d = [ ',j);
  for k = 1 : s(j)*N
    fprintf(fid, 'piecewise(t, t%d(%d), t%d(%d)) ', j, k, j, k + 1);
  end
  fprintf(fid, '];\n');
end
fprintf(fid, '\n');

fprintf(fid, '%% Block diagonal matrix\n');
for j = 1 : dim.m
  if j == 1
    fprintf(fid, 'P = [');
  else
    fprintf(fid, '     ');
  end
  for k = 1 : dim.m
    if j==k
      fprintf(fid, 'P%d         ',j);
    else
      fprintf(fid, 'zeros(1,%d) ', s(k)*N);
    end
  end
  if j ~= dim.m
    fprintf(fid, ';\n');
  end
end
fprintf(fid, '];\n\n');

fclose(fid);

% Creat mat_Pi
fid=fopen('mat_Pi.m', 'w');

fprintf(fid, 'function Pi = mat_Pi(t, prm)\n\n');

fprintf(fid, '%% Piecewise constant base\n');
fprintf(fid, '%%  t   - time\n');
fprintf(fid, '%%  prm - parameters\n\n');

fprintf(fid, '%% Start time\n');
fprintf(fid, 'Tb = prm.Tb;\n');
fprintf(fid, '%% Time horizon\n');
fprintf(fid, 'T  = prm.T;\n');
fprintf(fid, '%% Number of intervals\n');
for j = 1 : dim.m
  fprintf(fid, 'h%d = T/%d; \n', j, s(j));
end
fprintf(fid, '\n');

fprintf(fid, '%% Base function odffset\n');
for j = 1 : dim.m
  fprintf(fid, 't%d = Tb : h%d : T + Tb; \n', j, j);
end
fprintf(fid, '\n');

fprintf(fid, '%% Base matrix\n');
for j = 1 : dim.m
  fprintf(fid, 'P%d = [ ',j);
  for k = 1 : s(j)
    fprintf(fid, 'piecewise(t, t%d(%d), t%d(%d)) ', j, k, j, k + 1);
  end
  fprintf(fid, '];\n');
end
fprintf(fid, '\n');

fprintf(fid, '%% Block diagonal matrix\n');
for j = 1 : dim.m
  if j == 1
    fprintf(fid, 'Pi = [');
  else
    fprintf(fid, '      ');
  end
  for k = 1 : dim.m
    if j==k
      fprintf(fid, 'P%d         ',j);
    else
      fprintf(fid, 'zeros(1,%d) ', s(k));
    end
  end
  if j ~= dim.m
    fprintf(fid, ';\n');
  end
end
fprintf(fid, '];\n\n');

fclose(fid);

rehash path
base = 'const';
