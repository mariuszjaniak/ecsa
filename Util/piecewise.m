function y = piecewise(t, tb, te)
if (t >= tb) && (t < te)
  y = 1;
else
  y = 0;
end
