function out = vec2cell(in, sel, dim)

out    = cell(dim, 1);
for i = 1 : dim
  out{i} = in(sel{i});
end
